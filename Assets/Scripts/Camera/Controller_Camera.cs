﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Camera : MonoBehaviour
{
    public List<GameObject> players;
    private Camera _camera;
    public float dampTime = 0.15f;
    public float smoothTime = 2f;
    public float zoomValue;
    private Vector3 velocity = Vector3.zero;

    void Start()
    {
        _camera = GetComponent<Camera>();
    }

    void LateUpdate()
    {
        if (players[GameManager.actualPlayer] != null)
        {
            Vector3 targetPosition = players[GameManager.actualPlayer].transform.position;
            float camX = -16f;
            float camY = 0f;

            if (targetPosition.x > -16.0f)
            {
                if (targetPosition.y > -6.0f)
                {
                    camX = targetPosition.x;
                    camY = 0f;
                }
            }
            else if (targetPosition.x < -16.0f)
            {
                camX = -16f;
            }

            if (targetPosition.y < -5.0f)
            {
                if (targetPosition.x > 0.5f)
                {
                    camX = targetPosition.x;
                }
                else if (targetPosition.x < 15.30f)
                {
                    camX = 0.0f;
                }
                camY = -10f;
            }

            transform.position = Vector3.SmoothDamp(transform.position, new Vector3(camX, camY, zoomValue), ref velocity, dampTime);
        }
    }
}
