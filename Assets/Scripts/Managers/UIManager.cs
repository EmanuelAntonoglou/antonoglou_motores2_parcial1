using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public static UIManager i;

    public Controller_Camera mainCamera;
    public SpriteRenderer rubyUI;
    public SpriteRenderer emeraldUI;
    public SpriteRenderer sapphireUI;
    public Animator winAnimation;

    private void Awake()
    {
        CrearSingleton();
    }

    public void CrearSingleton()
    {
        if (i == null)
        {
            i = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }
}
