﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour
{
    void Update()
    {
        GetInput();
    }

    private void GetInput()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            GameManager.i.win = false;
            GameManager.i.winFlag = false;
            GameManager.i.gameOver = false;
            Time.timeScale = 1;
            SceneManager.LoadScene(0);
            GameManager.actualPlayer = 0;

            ResetUI();
            UIManager.i.winAnimation.Play("No Animation");
        }
    }

    private void ResetUI()
    {
        GameManager.ruby = false;
        ShineController rubyUI = UIManager.i.rubyUI.GetComponent<ShineController>();
        rubyUI.ChangeMaterial(rubyUI.sprMat, Color.white);
        rubyUI.InitializeMaterial();

        GameManager.emerald = false;
        ShineController emeraldUI = UIManager.i.emeraldUI.GetComponent<ShineController>();
        emeraldUI.ChangeMaterial(emeraldUI.sprMat, Color.white);
        emeraldUI.InitializeMaterial();

        GameManager.sapphire = false;
        ShineController sapphireUI = UIManager.i.sapphireUI.GetComponent<ShineController>();
        sapphireUI.ChangeMaterial(sapphireUI.sprMat, Color.white);
        sapphireUI.InitializeMaterial();
    }
}
