﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager i;

    public bool gameOver = false;
    public bool win = false;
    public bool winFlag = false;

    public static int actualPlayer = 0;

    public static bool redDoor = false;
    public static bool greenDoor = false;
    public static bool blueDoor = false;

    public static bool ruby = false;
    public static bool emerald = false;
    public static bool sapphire = false;


    private void Awake()
    {
        CrearSingleton();
    }

    public void CrearSingleton()
    {
        if (i == null)
        {
            i = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        Physics.gravity = new Vector3(0, -30, 0);
    }

    void Update()
    {
        GetInput();
        DetectWin();
    }

    private void GetInput()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            if (actualPlayer <= 0)
            {
                actualPlayer = 2;
            }
            else
            {
                actualPlayer--;
            }

        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (actualPlayer >= 2)
            {
                actualPlayer = 0;
            }
            else
            {
                actualPlayer++;
            }
        }
    }

    private void DetectWin()
    {
        if (!winFlag && redDoor && greenDoor && blueDoor)
        {
            winFlag = true;
            win = true;
        }
        if (win)
        {
            UIManager.i.winAnimation.Play("Win");
            win = false;
        }
    }

}
