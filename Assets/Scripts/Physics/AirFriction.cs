using UnityEngine;

public class AirFriction : MonoBehaviour
{
    [SerializeField] private float decelerationFactor = 0.1f;
    private Rigidbody2D rb;


    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        Vector2 currentVelocity = rb.velocity;
        Vector2 deceleration = -currentVelocity * decelerationFactor * Mathf.Exp(-Time.deltaTime);
        rb.AddForce(deceleration);
        rb.velocity = currentVelocity + deceleration;
    }
}

