using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{
    [Header("Door Data")]
    [SerializeField] private bool closedKey = false;

    [Header("References")]
    [SerializeField] private Sprite SPR_openDoor;
    [SerializeField] private Sprite SPR_closedDoor;
    private SpriteRenderer sprRenderer;


    private void Awake()
    {
        sprRenderer = GetComponent<SpriteRenderer>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            OpenKeyDoor(collision);
            OpenDoor(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            OpenDoor(false);
        }
    }

    private void OpenKeyDoor(Collider2D collision)
    {
        if (collision.gameObject.layer == 8 && collision.gameObject.GetComponent<RedPlayerController>().hasKey ||
            collision.gameObject.layer == 9 && collision.gameObject.GetComponent<GreenPlayerController>().hasKey ||
            collision.gameObject.layer == 10 && collision.gameObject.GetComponent<BluePlayerController>().hasKey)
        {
            if (closedKey)
            {
                closedKey = false;
                if (GameManager.actualPlayer == 0)
                {
                    Destroy(transform.parent.Find("Key R").gameObject);
                }
                else if (GameManager.actualPlayer == 1)
                {
                    Destroy(transform.parent.Find("Key G").gameObject);
                }
                else if (GameManager.actualPlayer == 2)
                {
                    Destroy(transform.parent.Find("Key B").gameObject);
                }
            }
        }
    }

    private void OpenDoor(bool opened)
    {
        if (!closedKey)
        {
            if (gameObject.layer == 8)
            {
                GameManager.redDoor = opened;
            }
            else if (gameObject.layer == 9)
            {
                GameManager.greenDoor = opened;
            }
            else if (gameObject.layer == 10)
            {
                GameManager.blueDoor = opened;
            }

            if (opened)
            {
                sprRenderer.sprite = SPR_openDoor;
            }
            else
            {
                sprRenderer.sprite = SPR_closedDoor;
            }
        }
    }

}
