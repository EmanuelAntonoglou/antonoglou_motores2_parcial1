using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyController : MonoBehaviour
{
    [SerializeField] private float followSpeed;
    [NonSerialized] public bool isFollowing = false;
    [NonSerialized] private Transform followTarget;

    private void Awake()
    {
        if (gameObject.layer == 8)
        {
            followTarget = transform.parent.parent.Find("Players").Find("Player Red").Find("Key Position").transform;
        }
        else if (gameObject.layer == 9)
        {
            followTarget = transform.parent.parent.Find("Players").Find("Player Green").Find("Key Position").transform;
        }
        else if (gameObject.layer == 10)
        {
            followTarget = transform.parent.parent.Find("Players").Find("Player Blue").Find("Key Position").transform;
        }
    }

    private void Update()
    {
        if (isFollowing)
        {
            transform.position = Vector3.Lerp(transform.position, followTarget.position, followSpeed * Time.deltaTime);
        }
    }
}
