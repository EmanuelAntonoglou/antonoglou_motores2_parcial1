using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreenPlayerController : RedPlayerController
{
    private void Awake()
    {
        UIManager.i.transform.Find("Main Camera").GetComponent<Controller_Camera>().players[1] = gameObject;
    }

    internal override void FixedUpdate()
    {
        if (GameManager.actualPlayer == playerNumber)
        {
            Movement();
        }
    }

    internal override void Movement()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");
        float horizontalVelocity = horizontalInput * speed;
        float verticalVelocity = verticalInput * speed;

        rb.velocity = new Vector2(horizontalVelocity, verticalVelocity);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Collectionable")
        {
            GameObject collectionable = collision.gameObject;
            if (collectionable.name == "Emerald")
            {
                Destroy(collectionable);
                GameManager.emerald = true;
                ShineController emeraldUI = UIManager.i.emeraldUI.GetComponent<ShineController>();
                emeraldUI.ChangeMaterial(emeraldUI.reflectionMat, emeraldUI.reflectionColor);
                emeraldUI.InitializeMaterial();
            }
        }
        if (collision.gameObject.tag == "Key")
        {
            KeyController key = collision.gameObject.GetComponent<KeyController>();
            hasKey = true;
            key.isFollowing = true;
        }
    }

}
