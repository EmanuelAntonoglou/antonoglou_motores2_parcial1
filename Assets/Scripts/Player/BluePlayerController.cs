using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BluePlayerController : RedPlayerController
{
    //[Header("Gravity Parameters")]
    //private bool canChangeGravity = true;

    private void Awake()
    {
        UIManager.i.transform.Find("Main Camera").GetComponent<Controller_Camera>().players[2] = gameObject;
    }

    internal override void FixedUpdate()
    {
        rb.AddForce(new Vector3(0, 30f, 0));

        if (GameManager.actualPlayer == playerNumber)
        {
            Movement();

            if (CheckGrounded() && Input.GetKey(KeyCode.W))
            {
                Jump();
            }
        }
    }

    internal override void Jump()
    {
        //canChangeGravity = false;
        transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y * -1, transform.localScale.z);

        if (rb.gravityScale == 1)
        {
            rb.gravityScale = -1;
            rb.AddForce(new Vector3(0, 30f, 0));
        }
        else if (rb.gravityScale == -1)
        {
            rb.gravityScale = 1;
            rb.AddForce(new Vector3(0, -30f, 0));
        }
    }

    internal override bool CheckGrounded()
    {
        Vector2 rayOrigin = new Vector2();
        RaycastHit2D hit = new RaycastHit2D();

        if (rb.gravityScale == -1)
        {
            raycastOffset = -0.71f;
            rayOrigin = new Vector2(transform.position.x, transform.position.y - raycastOffset) + Vector2.up * 0.1f;
            float rayLength = 0.1f;
            hit = Physics2D.Raycast(rayOrigin, Vector2.up, rayLength, allowedLayers);
        }
        else if (rb.gravityScale == 1)
        {
            raycastOffset = 0.425f;
            rayOrigin = new Vector2(transform.position.x, transform.position.y - raycastOffset) + Vector2.down * 0.1f;
            float rayLength = 0.1f;
            hit = Physics2D.Raycast(rayOrigin, Vector2.down, rayLength, allowedLayers);
        }

        if (hit.collider != null)
        {
            //canChangeGravity = true;
            return true;
        }
        return false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Collectionable")
        {
            GameObject collectionable = collision.gameObject;
            if (collectionable.name == "Sapphire")
            {
                Destroy(collectionable);
                GameManager.sapphire = true;
                ShineController sapphireUI = UIManager.i.sapphireUI.GetComponent<ShineController>();
                sapphireUI.ChangeMaterial(sapphireUI.reflectionMat, sapphireUI.reflectionColor);
                sapphireUI.InitializeMaterial();
            }
        }
        if (collision.gameObject.tag == "Key")
        {
            KeyController key = collision.gameObject.GetComponent<KeyController>();
            hasKey = true;
            key.isFollowing = true;
        }
    }

}
