﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedPlayerController : MonoBehaviour
{
    [Header("Base Movement Parameters")]
    [SerializeField] internal int playerNumber;
    [SerializeField] internal float jumpForce = 10;
    [SerializeField] internal float speed = 5;
    [SerializeField] internal float raycastOffset;
    [SerializeField] internal LayerMask allowedLayers;

    [Header("Objects Data")]
    internal bool hasKey = false;

    [Header("Components")]
    [NonSerialized] internal Rigidbody2D rb;
    [NonSerialized] internal SpriteRenderer sprRend;

    private void Awake()
    {
        UIManager.i.transform.Find("Main Camera").GetComponent<Controller_Camera>().players[0] = gameObject;
    }

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        sprRend = GetComponent<SpriteRenderer>();
    }

    internal virtual void FixedUpdate()
    {
        if (GameManager.actualPlayer == playerNumber)
        {
            Movement();

            if (CheckGrounded() && Input.GetKey(KeyCode.W))
            {
                Jump();
            }
        }
    }

    internal virtual void Movement()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float horizontalVelocity = horizontalInput * speed;

        if (Input.GetKey(KeyCode.A) && horizontalInput < 0)
        {
            transform.localScale = new Vector3(-1, transform.localScale.y, 1);
        }
        if (Input.GetKey(KeyCode.D) && horizontalInput >= 0)
        {
            transform.localScale = new Vector3(1, transform.localScale.y, 1);
        }
        rb.velocity = new Vector2(horizontalVelocity, rb.velocity.y);
    }

    internal virtual void Jump()
    {
        rb.velocity = new Vector2(rb.velocity.x, 0);
        rb.AddForce(new Vector2(0, jumpForce), ForceMode2D.Impulse);
    }

    internal virtual bool CheckGrounded()
    {
        Vector2 rayOrigin = new Vector2(transform.position.x, transform.position.y - raycastOffset) + Vector2.down * 0.1f;
        float rayLength = 0.1f;
        RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.down, rayLength, allowedLayers);

        if (hit.collider != null)
        {
            return true;
        }
        return false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Collectionable")
        {
            GameObject collectionable = collision.gameObject;
            if (collectionable.name == "Ruby")
            {
                Destroy(collectionable);
                GameManager.ruby = true;
                ShineController rubyUI = UIManager.i.rubyUI.GetComponent<ShineController>();
                rubyUI.ChangeMaterial(rubyUI.reflectionMat, rubyUI.reflectionColor);
                rubyUI.InitializeMaterial();
            }
        }
        if (collision.gameObject.tag == "Key")
        {
            KeyController key = collision.gameObject.GetComponent<KeyController>();
            hasKey = true;
            key.isFollowing = true;
        }
    }

}
