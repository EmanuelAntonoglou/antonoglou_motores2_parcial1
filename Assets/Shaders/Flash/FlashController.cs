using System;
using System.Collections;
using UnityEngine;

public class FlashController : MonoBehaviour
{
    [Header("Flash Data")]
    [SerializeField] private float flashTime = 2f;
    [NonSerialized] private Color originalColor;
    [NonSerialized] private bool changeMat = true;

    [Header("References")]
    [SerializeField] public Material flashMat;
    private SpriteRenderer sprRend;
    [NonSerialized] private Texture sprite;

    private void Awake()
    {
        sprRend = GetComponent<SpriteRenderer>();
        originalColor = sprRend.color;
    }

    private void Update()
    {
        if (changeMat)
        {
            ChangeMaterial(flashMat, Color.white);
            InitializeMaterial();
        }
    }

    public void InitializeMaterial()
    {
        sprite = sprRend.sprite.texture;
        sprRend.material.SetTexture("_MainTexture", sprite);
        sprRend.material.SetColor("_FlashColor", originalColor);
    }

    public void ChangeMaterial(Material mat, Color color)
    {
        sprRend.color = color;
        sprRend.material = mat;
    }

    private IEnumerator FlashSprite()
    {
        float elapsedTime = 0f;
        while (elapsedTime < flashTime)
        {
            float flashAmount = Mathf.Lerp(1f, 0f, elapsedTime / flashTime);
            sprRend.material.SetFloat("_FlashAmount", flashAmount);
            elapsedTime += Time.unscaledDeltaTime;
            yield return null;
        }
        sprRend.material.SetFloat("_FlashAmount", 0f);
        changeMat = false;
    }

    internal void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Spike")
        {
            StartCoroutine(FlashSprite());
            GameManager.i.gameOver = true;
            Time.timeScale = 0f;
        }
    }
}
