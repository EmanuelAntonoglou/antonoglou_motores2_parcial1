using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class OutlineController : MonoBehaviour
{
    [SerializeField] private Material outlineMat;
    [SerializeField] private Material sprMat;
    [NonSerialized] private Texture sprite;
    [NonSerialized] private Color originalColor;
    [NonSerialized] private Color outlineColor;

    [Header("References")]
    private SpriteRenderer sprRend;

    private void Awake()
    {
        sprRend = GetComponent<SpriteRenderer>();

        originalColor = sprRend.color;
    }

    private void Update()
    {
        if (GameManager.actualPlayer == 0 && gameObject.layer == 8 || // Red
            GameManager.actualPlayer == 1 && gameObject.layer == 9 || // Green
            GameManager.actualPlayer == 2 && gameObject.layer == 10 || // Blue
            GameManager.actualPlayer == 0 && gameObject.layer == 11 || // Yellow Red 
            GameManager.actualPlayer == 1 && gameObject.layer == 11 || // Yellow Green 
            GameManager.actualPlayer == 1 && gameObject.layer == 12 || // Cyan Green 
            GameManager.actualPlayer == 2 && gameObject.layer == 12 || // Cyan Blue 
            GameManager.actualPlayer == 0 && gameObject.layer == 13 || // Magenta Red
            GameManager.actualPlayer == 2 && gameObject.layer == 13 )  // Magenta Blue
        {
            ChangeMaterial(outlineMat, Color.white);
        }
        else
        {
            ChangeMaterial(sprMat, originalColor);
        }
        SetOutlineColor();
        InitializeMaterial();
    }

    private void InitializeMaterial()
    {
        sprite = sprRend.sprite.texture;
        sprRend.material.SetTexture("_MainTexture", sprite);
        sprRend.material.SetColor("_OutlineColor", outlineColor);
        sprRend.material.SetColor("_FillColor", originalColor);
    }

    private void ChangeMaterial(Material mat, Color color)
    {
        sprRend.color = color;
        sprRend.material = mat;
    }

    private void SetOutlineColor()
    {
        if (GameManager.actualPlayer == 0)
        {
            if (originalColor == new Color(1, 0, 0) ||
            originalColor == new Color(1, 1, 0) ||
            originalColor == new Color(1, 0, 1))
            {
                outlineColor = new Color(0.5f, 0.0f, 0.0f);
            }
        }
        else if (GameManager.actualPlayer == 1)
        {
            if (originalColor == new Color(0, 1, 0) ||
                 originalColor == new Color(1, 1, 0) ||
                 originalColor == new Color(0, 1, 1))
            {
                outlineColor = new Color(0.0f, 0.5f, 0.0f);
            }
        }
        else if (GameManager.actualPlayer == 2)
        {
            if (originalColor == new Color(0, 0, 1) ||
                 originalColor == new Color(1, 0, 1) ||
                 originalColor == new Color(0, 1, 1))
            {
                outlineColor = new Color(0.0f, 0.0f, 0.5f);
            }
        }
    }

}
